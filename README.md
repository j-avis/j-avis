
<h1 style="text-align: center;">JAVIS - Javascript Audio Visualizer</h1>
<br>
<img src="images/screen.jpg">
<br>
<p>Javis is a JavaScript based visualizer for audio. It is widely configurable and runs in every modern browser (firefox, chrome/opera, safari). This project also contains a standalone file which contains all code in one html file!
</p>
<a href="https://javis.flaffipony.rocks">Find more information here</a>