"use strict";

class Javis_Connector extends EventManager {
    
    constructor(core){
        super();

        this.core = core;

        this.key = undefined; // the connection key
        this.isActive = false; // if connector has an active connection or is trying to establish a connection
        this.mode = undefined; // 'sending' or 'receiving'

        this.core.ui.addControlGroup(
            [{
                hint: 'Connect',
                icon: 'connect',
                onclick: ()=>{
                    this.openConnectDialog();
                },
                afterBuild: (elem)=>{
                    this.controlElement = elem;
                }
            }]
        );

        this.dom = $('<div class="connecting_info"><span class="heading">Connecting ...</span><span class="key"></span></div>').hide();
        this.dom.appendTo(this.core.ui.dom);

        this.core.on('streamchange', (newStream)=>{
            if(this.mode === 'sending' && newStream && this.currentRtpSender){
                console.log('replacing connector track');
                this.currentRtpSender.replaceTrack(newStream.getAudioTracks()[0]);
            }
        })

        this.core.visualizer.on('tagschange', ()=>{
            if(this.isActive && this.mode === 'sending'){
                this.sub.sendDataMessage('tags', this.core.visualizer.currentTags);
            }
        })

        this.core.ui.configurator.on('change', ()=>{
            if(this.isActive && this.mode === 'sending'){
                this.sub.sendDataMessage('config', this.core.config);
            }
        })
    }

    openConnectDialog(){
        if(this.isActive){
            new Javis_Dialog({
                title: 'Cancel current connection?',
                type: Javis_Dialog.TYPE.OPTION_BUTTONS,
                options:[{
                    value: 'ok',
                    short: 'ok'
                }]
            }, this.core.ui.dom, (value)=>{
                if(value == 'ok'){
                    this.stop();
                    this.openConnectDialog();
                } else {
                    //canceled dialog
                }
            });
        } else {
            new Javis_Dialog({
                title: 'Select role of this window:',
                type: Javis_Dialog.TYPE.OPTION_BUTTONS,
                options:[{
                    value: 'sender',
                    short: 'sender',
                    description: 'Send data from this window.'
                },{
                    value: 'receiver',
                    short: 'receiver',
                    description: 'Connect to sender.'
                }]
            }, this.core.ui.dom, (value)=>{
                if(value == 'sender'){
                    this.startSending();
                } else if(value == 'receiver'){
                    this.openReceivingDialog();
                } else {
                    //canceled dialog
                }
            });
        }
    }

    openReceivingDialog(){
        new Javis_Dialog({
            title: 'Enter key of the connection:',
            type: Javis_Dialog.TYPE.INPUT,
            inputs:[{
                name: 'key',
                type: 'text',
                short: 'Key'
            }]
        }, this.core.ui.dom, (values)=>{
            if(values !== false && values.key){
                this.startReceiving(values.key);
            } else {
                //canceled dialog
            }
        });
    }

    refreshKey(){
        console.log('setting key to', this.key)
        this.dom.find('.key').text(this.key);
    }

    startSending(){
        this.key = undefined;
        this.isActive = true;
        this.mode = 'sending';

        this.sub = new Javis_Connector_Sender(this, (key)=>{
            this.key = key;
            this.refreshKey();
        });
        
        let once = true;

        let fakeAudio = document.createElement('audio');
        fakeAudio.srcObject = this.core.outputStreamNode.stream;
        //fakeAudio.muted = true;

        this.sub.on('connected', ()=>{
            if(once){
                console.info('adding stream track to connector', this.core.outputStreamNode.stream);
                once = false;

                let tracks = this.core.outputStreamNode.stream.getAudioTracks();
                let track = this.core.outputStreamNode.stream.getAudioTracks()[0]
                this.currentRtpSender = this.sub.connection.addTrack(track, this.core.outputStreamNode.stream);
            }
        });

        this.setupConnectingInfo();
    }

    startReceiving(key){
        this.key = key;
        this.isActive = true;
        this.mode = 'receiving';

        this.refreshKey();

        this.sub = new Javis_Connector_Receiver(this, this.key);
        
        this.sub.connection.ontrack = (evt)=>{
            console.info('connector track added', evt.streams[0]);
            this.core.startConnectorReceiving(evt.streams[0]);

            //TODO temporary fix
            let audioEl = document.createElement('audio');
            audioEl.srcObject = evt.streams[0];
        }

        this.sub.on('message', (data)=>{
            switch(data.type){
                case 'tags': {
                    this.core.visualizer.setId3Tags(data.content);
                }; break;
                case 'config': {
                    let newConfig = this.core.ui.configurator.configify(data.content);
                    newConfig.setValue('volume', this.core.config.getValue('volume'));//preserve client volume setting
                    this.core.ui.configurator.setConfig(newConfig);
                }; break;
            }
        })

        this.setupConnectingInfo();
    }

    stop(){
        this.sub.close();
        this.sub = undefined;

        this.currentRtpSender = undefined;

        this.isActive = false;
        this.mode = undefined;

        if(this.controlElement){
            this.controlElement.removeAttr('connect-state');
        }
    }

    setupConnectingInfo(){
        this.showConnectingInfo();

        this.sub.on('connected', ()=>{
            this.connectSuccess(()=>{
                this.hideConnectingInfo();

                if(this.controlElement){
                    this.controlElement.attr('connect-state', 'success');
                }
            });
        });

        this.sub.on('disconnected', ()=>{
            this.showConnectingInfo();
            this.connectFail();

            if(this.controlElement){
                this.controlElement.attr('connect-state', 'fail');
            }
        });

        if(this.controlElement){
            this.controlElement.attr('connect-state', 'pending');
        }
    }

    connectSuccess(callback){
        this.dom.addClass('success');
        setTimeout(()=>{
            this.dom.removeClass('success');
            if(typeof callback === 'function'){
                callback();
            }
        }, 1000);
    }

    connectFail(callback){
        this.dom.addClass('fail');
        setTimeout(()=>{
            this.dom.removeClass('fail');
            if(typeof callback === 'function'){
                callback();
            }
        }, 1000);
    }

    hideConnectingInfo(){
        this.dom.hide();
    }

    showConnectingInfo(){
        this.dom.show();
    }

    sendBackgroundBlob(blobUrl, type){
        this.sendMedia('background|' + type, blobUrl);
    }

    sendMedia(type, data){
        if(this.mode === 'sending'){
            this.sub.sendDataMedia(type, data);
        }
    }

    onReceiveMediaTransmission(type, data){
        let maintype = type.split('|')[0];
        let subtype = type.split('|')[1];
        switch(maintype){
            case 'background' : {
                this.core.visualizer.setBackgroundMedia(new Blob([data], {type: subtype}));
            }; break;

            default : {
                console.log('unsupported media transmission type', type);
            }
        }
    }
}
