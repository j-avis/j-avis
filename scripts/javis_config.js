"use strict";

class Javis_Configurator extends EventManager {
    
    constructor(ui){
        super();
        this.ui = ui;
        this.core = ui.core;

        let settingsHead = $('<div class="javis_settings_head">');
        settingsHead.appendTo(this.ui.settingsContainer);

        let settingsToggler = $('<div class="javis_settings_toggler">Hide settings<span class="version">v'+ Javis.VERSION +'</span></div>');
        settingsToggler.appendTo(settingsHead);
        settingsToggler.on('click', ()=>{
            this.ui.closeSettings();
        });

        let settingsButtons = $('<div class="javis_settings_group javis_settings_buttons_container">');
        settingsButtons.appendTo(settingsHead);

        for(let b of [
            {
                icon: 'update',
                onclick: ()=>{
                    this.updateConfig();
                },
                title: 'Update Configuration'
            },{
                icon: 'load',
                onclick: ()=>{
                    this.ui.openLoadConfigDialog();
                },
                title: 'Load config from browser cache'
            },{
                icon: 'save',
                onclick: ()=>{
                    this.ui.openSaveConfigDialog();
                },
                title: 'Save config in browser cache'
            }
        ]){
            let elem = $('<span class="javis_settings_icon">').attr('title', b.title).addClass('javis_icon_' + b.icon);
            elem.on('click', b.onclick);
            elem.appendTo(settingsButtons);
        }

        let setgroup = $(
            '<div class="javis_settings_group">'+                    
                '<span>Detailed configuration:</span><br>'+
            '</div>'
        );
        setgroup.appendTo(this.ui.settingsContainer);

        this.ui.settingsContainer.css('max-height', $(this.ui.dom).height());

        this.showConfig(this.core.config);

        console.log('config', this.core.config);
    }

    updateConfig(new_config){        
        console.log('updating config...')
        let count = 0
        $(this.ui.settingsContainer).find('.settings_table').find('input').each((index, elem)=>{
            let name = $(elem).data().avisname
            let type = $(elem).attr('type')     
            let value
            switch (type) {
                case 'range':
                case 'number': value = $(elem).val().replace(/[^0-9-,\.]/g, '')
                    break;
                case 'checkbox': value = $(elem).get()[0].checked
                    break;
                default: value = $(elem).val()
            }
            let parsed_value
            try {
                parsed_value = JSON.parse(value)
            } catch (ex) {
                parsed_value = value
            }

            if(! this.core.config.setValue(name, parsed_value)){
                this.showMessage('failed to set param', name, 'to', parsed_value)
                console.warn('failed to set param', name, 'to', parsed_value)
            } else {
                console.log('setting "'+name+'" to ', value)
                count++
            }
        })
        console.log(count+' config values updated')
        this.showConfig(this.core.config)

        this.dispatch('change');
    }

    setConfig(config){
        console.log('setting config... to', config)
        if(! config instanceof Javis_Config){
            console.error('invalid new config. must be an instance of Javis_Config! But was', config)
            return
        }
        this.core.config = config
        this.refresh();

        this.dispatch('change');
    }

    refresh(){
        this.showConfig(this.core.config);
    }

    showConfig(config){
        let table = $('<div class="javis_settings_group settings_table">');
        let left = $('<div class="left">');
        left.appendTo(table);
        let right = $('<div class="right">');
        right.appendTo(table);

        for(let key of Object.keys(config.params)){
            let param = config.params[key];

            if(param.is_hidden) break;

            let type;
            switch(typeof param.getConfigValue()){
                case 'number': type='number';break;
                case 'boolean': type='checkbox';break;
                default: type='text';
            }

            left.append(
                $('<div><label title="'+(Javis_ConfigDescriptions[param.name]?Javis_ConfigDescriptions[param.name]:'')+'">'+param.name+'</label></div>')
            )

            right.append(
                $('<div><input type="'+type+'" value="'+param.getConfigValue()+'" '+(param.getConfigValue()===true?'checked="true"':'')+' data-avisname="'+param.name+'"/></div>')
            )
        }

        this.ui.settingsContainer.find('.settings_table').remove();
        this.ui.settingsContainer.append(table);
    }

    selectPreset(name){
        if (Javis_Config[name.toUpperCase()]){
            this.setConfig(Javis_Config[name.toUpperCase()].clone())
        } else {            
            console.error('unknown preset', name, 'valid presets:', this.getPresetList())
        }
    }

    getPresetList(){
        let presets = []
        for(let key of Object.keys(Javis_Config)){
            presets.push(key)
        }
        return presets
    }


    checkLocalStorageAvailability(){
        try {
            localStorage.avis = true
            return true
        } catch (ex) {
            return false
        }
    }

    configify(config_obj){
        if(!config_obj.params){
            console.error('error configifying: config_obj has no params')
            return false
        }
        let pars = []
        for(let key of Object.keys(config_obj.params) ){
            let param = config_obj.params[key]
            pars.push(new Javis_Param(param))
        }
       let avis_conf = new Javis_Config(pars) 
       console.log('configified ', config_obj, 'to', avis_conf)
       return avis_conf
    }

    saveLocalConfig(avis_config, name){
         if(!this.checkLocalStorageAvailability()){
            console.error('browser not supporting/allowing localStorage, are you in localfile mode?')
            this.showMessage('browser not supporting/allowing localStorage, are you in localfile mode?')
            return false
        }
        try {
            let configs = this.getLocalConfigs()
            if(!configs) configs = []
            let new_configs = []
            for(let cf of configs){
                new_configs.push({name: cf.name, version: cf.version, config_obj: JSON.stringify(cf.the_config)})
            }
            let json_str = JSON.stringify(avis_config)
            new_configs.push({name: name, version: Javis.VERSION, config_obj: json_str})
            localStorage.setItem('javis_local_configs', JSON.stringify( new_configs ))
            return true
        } catch (ex){
            console.error('Cant save local configs', ex)
            this.showMessage('Cant save local configs')
            return false
        }
    }

    getLocalConfigs(){
        if(!this.checkLocalStorageAvailability()){
            console.error('browser not supporting/allowing localStorage, are you in localfile mode?')
            this.showMessage('browser not supporting/allowing localStorage, are you in localfile mode?')
            return false
        }
        try {
            let ret = []
            let configs
            try{
                configs = JSON.parse(localStorage.getItem('javis_local_configs'))
                if(!configs instanceof Array || !configs.length) configs = []
            } catch(ex){
                configs = []
            }
            for(let config of configs){
                try {
                    let parsed = this.configify(JSON.parse(config.config_obj))
                    if(parsed !== false){
                        if(config.version && config.version === Javis.VERSION){
                            ret.push({name: config.name, version: config.version, the_config: parsed})
                        } else {
                            console.log('skipping outdated local config "'+config.name+'". Current version: '+Javis.VERSION+' Config version: '+(config.version?config.version:'unknown'))
                            this.showMessage('skipping outdated local config "'+config.name+'". Current version: '+Javis.VERSION+' Config version: '+(config.version?config.version:'unknown'))
                        }
                    }
                } catch (ex){

                }
            }
            return ret
        } catch (ex){
            console.error('Cant load local configs', ex)
            this.showMessage('Cant load local configs')
            return false
        }
    }
}








/* Helper Classes */

class Javis_Param {
    constructor(p){/*{name, value, modifier, is_hidden}*/
        if(typeof p.name !== 'string' || typeof p.value === 'undefined'){
            console.error('missing param name or value!')
            return null
        }

        if(typeof p.value === 'function' && typeof p.modifier === 'undefined'){
            console.error('if you specify a function as value then you have to pass the modifier too!')
            return null
        }

        this.name = p.name
        this.value = p.value
        this.modifier = p.modifier
        this.is_hidden = p.is_hidden === true?true:false
    }

    getValue(instance){
        if(typeof this.modifier !== 'undefined'){
            return this.value*( instance.visualizer.canvasMin ? instance.visualizer.canvasMin : 1 ) * this.modifier
        } else {
            return this.value
        }
    }

    setValue(new_val){
        this.value = new_val
        return true
    }

    getConfigValue(){
        return this.value
    }

    clone(){
        return new Javis_Param({name: this.name, value: this.value, modifier: this.modifier, is_hidden: this.is_hidden})
    }
}

class Javis_Config extends EventManager {
    constructor(params/* [Javis_Param,...] */){
        super();
        if(!params || !params.length){
            console.error('missing Javis_Params!')
            return null
        }

        this.params = {}
        for(let param of params){
            if(! param instanceof Javis_Param){
                console.warn('skipping param', param, 'because its not a Javis_Param.')
                break
            }
            this.params[param.name] = param
        }
    }

    getParam(param_name){
        if(!!this.params[param_name]){
            return this.params[param_name]
        } else {
            console.error('param with name', name, 'not found in this config')
        }
    }

    getValue(param_name, instance){
        if(!!this.params[param_name]){
            return this.params[param_name].getValue(instance)
        } else {
            console.error('param with name', param_name, 'not found in this config')
        }
    }

    getConfigValue(param_name){
        if(!!this.params[param_name]){
            return this.params[param_name].getConfigValue()
        } else {
            console.error('param with name', param_name, 'not found in this config')
        }
    }

    setValue(param_name, value){
        if(!!this.params[param_name]){
            let ret = this.params[param_name].setValue(value);

            this.dispatch('change');

            return ret;
        } else {
            console.error('param with name', param_name, 'not found in this config')
            return false
        }
    }

    clone(){
        let newParams = []
        for(let key of Object.keys(this.params)){
            let newParam = this.params[key].clone()
            newParams.push(newParam)
        }
        //console.log('old params', this.params, 'newly cloned params', newParams)
        return new Javis_Config(newParams)
    }

}

const Javis_ConfigDescriptions = {
    volume: 'the output volume (0-1)',
    radius: 'radius of the inner circle',
    value_multiplier: 'length of lines',
    type: 'type of visualization (lines, graph, graph_small, graph_and_lines)',
    smoothness: 'smoothes the lines over time (0: no smooth, 1: max smooth)',
    line_length_min: 'minimum length of the lines',
    number_of_lines: 'affects the resolution of the sound analysis. In lines you affect the line amount, in graph you affect the graph smoothness',
    max_frequency: 'visualization ranges from 0 to this value in Hz',
    line_smoothness: 'how smooth the difference between neighbor values is',
    line_width: 'the width of lines or graph',
    color_single_line: 'make the colorizing append to a single line and not over the complete page',
    offset_left: 'offset in deg the left lines/graph has relative to the right middle (which is 0 deg in kartasian coordinatesystem) of the inner circle',
    offset_right: 'same as offset_left but for the right side of the lines/graph',
    left_and_right_separation: 'select left side as left channel and rigt side as right channel (most files wont really have a difference)',
    bass_multiplier: 'upon bass multiply the radius with a multiplier (0 for no bassing)',
    bass_upper_limit: 'upper limit in Hz what counts into the real bass value',
    bass_smash: 'if the inner circle text should smash with the bass (only works good with basslanes this come and go, and not have 100%bass through the whole file....)',
    bass_smash_limit: 'limit above this the smash will activate (0: always, 0.9: when bass is above 90% of max volume)',
    bass_average: 'calculate bass by taking the average of the frequency values up to the bass limit (if not it takes the frequency you defined in bass_upper_limit)',
    mark_max: 'adds a red mark (like on old audio stations) when the value is at max',
    mark_max_limit: 'limit above this values will be marked (0: alway, 0.9: when value is above 90% of max volume)',
    record_output_gain: 'the output volume of the sound this is recorded (0: mute, 1: hear yourself in max volume)',
    adjust_font_size: 'automatically adjusts the font size of the inner circle to make it fit',
    show_tags: 'enables id3 tags or similiar in the inner circle',
    tags_text_width: 'width of the text in the inner circle (0.8: 80% of the whole space in the inner circle)',
    tags_text_allow_wrap: 'allow text to wrap inside the circle',
    tags_text_font_size: 'font-size of the text in the inner circle (may get overridden by "adjust_font_size")'
}

/* COLORS 
const BLUEISH = {
    0: '#619ea3',
    0.5: '#619ea3',
    1: '#52D2BB'
}
const BLUEGREEN = [
    '#005555',
    '#198484',
    '#b2d6d6'
]
const DARK = {
    0: '#191919',
    1: '#000000'
}
const WHITE = {
    0: '#e5e5e5',
    0.5: '#f2f2f2',
    1: '#ff
    ffff'
}*/

Javis_Config.LINEAR = new Javis_Config([
    new Javis_Param({name: 'volume', value: 1}),
    new Javis_Param({name: 'radius', value: 240, modifier: 0.001 }),
    new Javis_Param({name: 'value_multiplier', value: 60, modifier: 0.00001 }),
    new Javis_Param({name: 'type', value: 'linear'}),
    new Javis_Param({name: 'smoothness', value: 0.75}),
    new Javis_Param({name: 'line_length_min', value: 0 }),
    new Javis_Param({name: 'number_of_lines', value: 150}),
    new Javis_Param({name: 'max_frequency', value: 500}),
    new Javis_Param({name: 'line_smoothness', value: 2}),
    new Javis_Param({name: 'line_width', value: 3}),
    new Javis_Param({name: 'color_single_line', value: true}),
    new Javis_Param({name: 'offset_left', value: -89.65 }),
    new Javis_Param({name: 'offset_right', value: 89.65 }),
    new Javis_Param({name: 'left_and_right_separation', value: true}),
    new Javis_Param({name: 'bass_multiplier', value: 0, modifier: 0.0001 }),
    new Javis_Param({name: 'bass_upper_limit', value: 0}),
    new Javis_Param({name: 'bass_smash', value: false}),        
    new Javis_Param({name: 'bass_smash_limit', value: 0}),
    new Javis_Param({name: 'bass_average', value: false }),
    new Javis_Param({name: 'mark_max', value: true }),
    new Javis_Param({name: 'mark_max_limit', value: 0.8 }),
    new Javis_Param({name: 'adjust_font_size', value: true}),
    new Javis_Param({name: 'show_tags', value: true}),
    new Javis_Param({name: 'tags_text_width', value: 0.8}),
    new Javis_Param({name: 'tags_text_allow_wrap', value: true}),
    new Javis_Param({name: 'tags_text_font_size', value: 16}),
    new Javis_Param({name: 'background_url', value: 'images/preset_linear_background.jpg', is_hidden: true}),
    new Javis_Param({name: 'color_background', value: '#000000', is_hidden: true}),
    new Javis_Param({name: 'color_background_type', value: 'linear', is_hidden: true}),
    new Javis_Param({name: 'color_line', value: ['#92ffaf','    #56ffa5','#00ff9f'], is_hidden: true}),
    new Javis_Param({name: 'color_max', value: '#ffffffff', is_hidden: true})
])




Javis_Config.LINES = new Javis_Config([
    new Javis_Param({name: 'volume', value: 1}),
    new Javis_Param({name: 'radius', value: 120, modifier: 0.001 }),
    new Javis_Param({name: 'value_multiplier', value: 10, modifier: 0.00001 }),
    new Javis_Param({name: 'type', value: 'lines'}),
    new Javis_Param({name: 'smoothness', value: 0}),
    new Javis_Param({name: 'line_length_min', value: 2 }),
    new Javis_Param({name: 'number_of_lines', value: 200}),
    new Javis_Param({name: 'max_frequency', value: 500}),
    new Javis_Param({name: 'line_smoothness', value: 0}),
    new Javis_Param({name: 'line_width', value: 1}),
    new Javis_Param({name: 'color_single_line', value: false}),
    new Javis_Param({name: 'offset_left', value: -89.65 }),
    new Javis_Param({name: 'offset_right', value: 89.65 }),
    new Javis_Param({name: 'left_and_right_separation', value: true}),
    new Javis_Param({name: 'bass_multiplier', value: 1, modifier: 0.0001 }),
    new Javis_Param({name: 'bass_upper_limit', value: 50}),
    new Javis_Param({name: 'bass_smash', value: true}),        
    new Javis_Param({name: 'bass_smash_limit', value: 0.9}),
    new Javis_Param({name: 'bass_average', value: true }),
    new Javis_Param({name: 'mark_max', value: true }),
    new Javis_Param({name: 'mark_max_limit', value: 0.8 }),
    new Javis_Param({name: 'adjust_font_size', value: true}),
    new Javis_Param({name: 'show_tags', value: true}),
    new Javis_Param({name: 'tags_text_width', value: 0.8}),
    new Javis_Param({name: 'tags_text_allow_wrap', value: true}),
    new Javis_Param({name: 'tags_text_font_size', value: 16}),
    new Javis_Param({name: 'background_url', value: 'images/preset_lines_background.jpg', is_hidden: true}),
    new Javis_Param({name: 'color_background', value: '#000000', is_hidden: true}),
    new Javis_Param({name: 'color_background_type', value: 'linear', is_hidden: true}),
    new Javis_Param({name: 'color_line', value: ['#e5e5e5','#f2f2f2','#ffffff'], is_hidden: true}),
    new Javis_Param({name: 'color_max', value: '#008080', is_hidden: true})
])


Javis_Config.DEFAULT = Javis_Config.LINES

Javis_Config.GRAPH = new Javis_Config([
    new Javis_Param({name: 'volume', value: 1}),
    new Javis_Param({name: 'radius', value: 140, modifier: 0.001 }),
    new Javis_Param({name: 'value_multiplier', value: 15, modifier: 0.00001 }),
    new Javis_Param({name: 'type', value: 'graph'}),
    new Javis_Param({name: 'smoothness', value: 0.75}),
    new Javis_Param({name: 'line_length_min', value: 2 }),
    new Javis_Param({name: 'number_of_lines', value: 150}),
    new Javis_Param({name: 'max_frequency', value: 500}),
    new Javis_Param({name: 'line_smoothness', value: 2}),
    new Javis_Param({name: 'line_width', value: 5}),
    new Javis_Param({name: 'color_single_line', value: false}),
    new Javis_Param({name: 'offset_left', value: -89.65 }),
    new Javis_Param({name: 'offset_right', value: 89.65 }),
    new Javis_Param({name: 'left_and_right_separation', value: true}),
    new Javis_Param({name: 'bass_multiplier', value: 2, modifier: 0.0001 }),
    new Javis_Param({name: 'bass_upper_limit', value: 50}),
    new Javis_Param({name: 'bass_smash', value: true}),        
    new Javis_Param({name: 'bass_smash_limit', value: 0.9}),
    new Javis_Param({name: 'bass_average', value: true }),
    new Javis_Param({name: 'mark_max', value: false }),
    new Javis_Param({name: 'mark_max_limit', value: 0.8 }),
    new Javis_Param({name: 'adjust_font_size', value: true}),
    new Javis_Param({name: 'show_tags', value: true}),
    new Javis_Param({name: 'tags_text_width', value: 0.8}),
    new Javis_Param({name: 'tags_text_allow_wrap', value: true}),
    new Javis_Param({name: 'tags_text_font_size', value: 20}),
    new Javis_Param({name: 'background_url', value: 'images/preset_graph_background.jpg', is_hidden: true}),
    new Javis_Param({name: 'color_background', value: '#000000', is_hidden: true}),
    new Javis_Param({name: 'color_background_type', value: 'radial', is_hidden: true}),
    new Javis_Param({name: 'color_line', value: ['#740001','#eeba30','#7E6316'], is_hidden: true}),
    new Javis_Param({name: 'color_max', value: '#ff0000', is_hidden: true})
])

Javis_Config.GRAPH_AND_LINES = new Javis_Config([
    new Javis_Param({name: 'volume', value: 1}),
    new Javis_Param({name: 'radius', value: 120, modifier: 0.001 }),
    new Javis_Param({name: 'value_multiplier', value: 15, modifier: 0.00001 }),
    new Javis_Param({name: 'type', value: 'graph_with_lines'}),
    new Javis_Param({name: 'smoothness', value: 0.75}),
    new Javis_Param({name: 'line_length_min', value: 3 }),
    new Javis_Param({name: 'number_of_lines', value: 80}),
    new Javis_Param({name: 'max_frequency', value: 500}),
    new Javis_Param({name: 'line_smoothness', value: 2}),
    new Javis_Param({name: 'line_width', value: 4}),
    new Javis_Param({name: 'color_single_line', value: false}),
    new Javis_Param({name: 'offset_left', value: -89 }),
    new Javis_Param({name: 'offset_right', value: 89 }),
    new Javis_Param({name: 'left_and_right_separation', value: true}),
    new Javis_Param({name: 'bass_multiplier', value: 1, modifier: 0.0001 }),
    new Javis_Param({name: 'bass_upper_limit', value: 50}),
    new Javis_Param({name: 'bass_smash', value: true}),        
    new Javis_Param({name: 'bass_smash_limit', value: 0.9}),
    new Javis_Param({name: 'bass_average', value: true }),
    new Javis_Param({name: 'mark_max', value: false }),
    new Javis_Param({name: 'mark_max_limit', value: 0.8 }),
    new Javis_Param({name: 'adjust_font_size', value: true}),
    new Javis_Param({name: 'show_tags', value: true}),
    new Javis_Param({name: 'tags_text_width', value: 0.8}),
    new Javis_Param({name: 'tags_text_allow_wrap', value: true}),
    new Javis_Param({name: 'tags_text_font_size', value: 16}),
    new Javis_Param({name: 'background_url', value: 'images/preset_graph_and_lines_background.jpg', is_hidden: true}),
    new Javis_Param({name: 'color_background', value: '#000000', is_hidden: true}),
    new Javis_Param({name: 'color_background_type', value: 'single', is_hidden: true}),
    new Javis_Param({name: 'color_line', value: ['#810c14','#f08c8a','#810c14'], is_hidden: true}),
    new Javis_Param({name: 'color_max', value: '#ffe700', is_hidden: true})
])