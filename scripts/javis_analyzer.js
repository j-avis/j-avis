"use strict";

class Javis_Analyzer {
    
    constructor(visualizer){
        this.visualizer = visualizer;
        this.core = visualizer.core;

        this.WANTED_FREQUENCY_RANG_PER_VALUE = 3;

        let totalFrequencyRange = this.core.audioContext.sampleRate / 2;
        let wantedFftSize = (totalFrequencyRange / this.WANTED_FREQUENCY_RANG_PER_VALUE) * 2;

        const FFT_SIZE = this.getNextHigherPowerOfTwo(wantedFftSize);
        console.log('Calculated fft size', FFT_SIZE);

        this.analyzerL = this.core.audioContext.createAnalyser();
        this.analyzerL.fftSize = FFT_SIZE;

        this.analyzerR = this.core.audioContext.createAnalyser();
        this.analyzerR.fftSize = FFT_SIZE;

        this.analyzerBass = this.core.audioContext.createAnalyser();
        this.analyzerBass.fftSize = FFT_SIZE;

        this.bassFilter = this.core.audioContext.createBiquadFilter()
        this.bassFilter.type = "lowpass";

        this.core.audioSourceNode.connect(this.bassFilter);
        this.bassFilter.connect(this.analyzerBass);

        this.core.ui.configurator.on('change', ()=>{
            this.updateSettings();
        });
        this.updateSettings();
    }

    updateSettings(){
        /* smoothing */
        this.analyzerL.smoothingTimeConstant = this.core.getConfig('smoothness'); //0=>no lag, 1=>kind of laggy
        this.analyzerR.smoothingTimeConstant = this.core.getConfig('smoothness');
        this.analyzerBass.smoothingTimeConstant = 0.75;        

        /* left and right separation */
        let doSeperate = this.core.getConfig('left_and_right_separation');

        if(doSeperate && this.channelSplitter || !doSeperate && !this.channelSplitter){
            //same setup, no need to change anything
        } else {
            try {
                if(this.channelSplitter){
                    this.core.audioSourceNode.disconnect(this.channelSplitter);
                    this.channelSplitter = undefined;
                } else {
                    this.core.audioSourceNode.disconnect(this.analyzerL);
                    this.core.audioSourceNode.disconnect(this.analyzerR);
                }
            } catch (thrown){}

            if(doSeperate){
                this.channelSplitter = this.core.audioContext.createChannelSplitter(2);
                this.channelSplitter.connect(this.analyzerL, 0);
                this.channelSplitter.connect(this.analyzerR, 1);
                this.core.audioSourceNode.connect(this.channelSplitter);
            } else {
                this.core.audioSourceNode.connect(this.analyzerL);
                this.core.audioSourceNode.connect(this.analyzerR);
            }
        }

        /* bass upper limit */
        this.bassFilter.frequency.value = this.core.getConfig('bass_upper_limit');
    }

    analyzeAudio(){
        // get the average for the first channel
        let arrayL = new Uint8Array(this.analyzerL.frequencyBinCount);//min: 0, max: 255
        this.analyzerL.getByteFrequencyData(arrayL);

        let arrayR = new Uint8Array(this.analyzerR.frequencyBinCount);//min: 0, max: 255
        this.analyzerR.getByteFrequencyData(arrayR);

        let arrayBass = new Uint8Array(this.analyzerBass.frequencyBinCount);//min: 0, max: 255
        this.analyzerBass.getByteFrequencyData(arrayBass);

        // cutoff data if above max_frequency
        let maxArraySize = Math.ceil(this.core.getConfig('max_frequency') / this.WANTED_FREQUENCY_RANG_PER_VALUE);
        arrayL = arrayL.subarray(1, maxArraySize + 1);
        arrayR = arrayR.subarray(1, maxArraySize + 1);

        let maxBassArraySize = Math.ceil(this.core.getConfig('bass_upper_limit') / this.WANTED_FREQUENCY_RANG_PER_VALUE);
        arrayBass = arrayBass.subarray(1, maxArraySize + 1);



        //TODO improve bass detection. Maybe self tuning algorithm?

        let bass
        if(this.core.getConfig('bass_average')){
            bass = Math.floor(this.average(arrayBass)*3)
        } else {
            bass = Math.floor(arrayBass[parseInt(this.core.getConfig('bass_upper_limit')/this.WANTED_FREQUENCY_RANG_PER_VALUE)])
        }

        return {
            l: arrayL,
            r: arrayR,
            bass: bass
        }
    }

    average(array){
        let sum = 0;
        for(let a of array){
            sum += a;
        }
        return sum / array.length;
    }

    getNextHigherPowerOfTwo(wanted){
        let current = 1;
        while(2**current < wanted){
            current++;
        }

        return 2**current;
    }
}